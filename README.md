<h1> HPS Encoder
    
Содержит в себе исходники для вывода данных с квадратурного энкодера через 
32bit F2H bridge в Linux


<h2> Компиляция Linux части 

    1. Открыть SoC EDS Command Shell
    2. Добавить путь, где <c/intelFPGA_lite...> - путь к sopc-create-header-files
        export PATH=$PATH:/cygdrive/<c/intelFPGA_lite/18.1/quartus/sopc_builder/bin>
    3. Запустить generate.sh
    4. Добавить в MakeFile необходимые исходники и изменения
    5. Вызвать make 
    6. Полученный файл перенести на флешпамять устройства, например, через SCP.