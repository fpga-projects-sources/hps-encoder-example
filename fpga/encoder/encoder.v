`ifndef ENCODER_V
`define ENCODER_V

module Encoder(input wire clk,
               input wire en_a,
               input wire en_b,
               output reg [9:0] counter);

    reg [2:0] en_a_delayed;
    reg [2:0] en_b_delayed;
    
    wire count_enable = en_a_delayed[1] ^ en_a_delayed[2] ^ 
                        en_b_delayed[1] ^ en_b_delayed[2];

    wire count_direction = en_a_delayed[1] ^ en_b_delayed[2];

    always @ (posedge clk)
        en_a_delayed <= {en_a_delayed[1:0], en_a};

    always @ (posedge clk)
        en_b_delayed <= {en_b_delayed[1:0], en_b};

    always @ (posedge clk)
    begin
        if(count_enable)
            if(count_direction)
                counter <= counter + 1;
            else
                counter <= counter - 1;
    end

endmodule

`endif