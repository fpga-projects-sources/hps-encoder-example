#include <stdio.h>
#include <unistd.h>
#include <fcntl.h> 
#include <sys/mman.h> 
#include "hwlib.h" 
#include "socal/socal.h"
#include "socal/hps.h" 
#include "socal/alt_gpio.h"
#include "hps_0.h"

#define REG_BASE 0xFF200000
#define REG_SPAN 0x00200000

void* virtual_base;
void* led_addr;
void* en_addr;
int fd;
int en_value;

int main ()
{
    fd=open("/dev/mem",(O_RDWR|O_SYNC));
    virtual_base=mmap(NULL,REG_SPAN,(PROT_READ|PROT_WRITE),MAP_SHARED,fd,REG_BASE);
    en_addr=virtual_base+ENCODER_PIO_BASE ;

    while(1)
    {
        en_value=*(uint32_t *)en_addr;
        usleep(10000);
        printf("%u\n",en_value);



    }
return 0;
}